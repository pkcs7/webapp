/*
 *  PKCS #7 App - demonstrate PKCS #7 manipulation using WebAssembly
 *  Copyright (C) 2021 Frank Engler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Org.BouncyCastle.Cms;

namespace webapp
{
    internal class SignatureStore : ISignatureStore
    {
        private List<Signature> _signatures;

        public SignatureStore()
        {
            _signatures = new();
        }

        public async Task AddSignatureFileAsync(IBrowserFile file)
        {
            var signature = await Signature.CreateAsync(file);
            _signatures.Add(signature);
        }

         public byte[] MergeSignatures()
         {
             CmsSignedDataGenerator generator = new();
             foreach (var signature in Signatures)
             {
                 if (signature.isSignature)
                 {
                    generator.AddCertificates(signature.SignatureData.GetCertificates("Collection"));
                    generator.AddSigners(signature.SignatureData.GetSignerInfos());
                 }
             }
             return generator.Generate(new CmsProcessableByteArray(new byte[0])).GetEncoded();
         }

        public List<Signature> Signatures { get => _signatures; }
    }
}