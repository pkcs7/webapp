/*
 *  PKCS #7 App - demonstrate PKCS #7 manipulation using WebAssembly
 *  Copyright (C) 2021 Frank Engler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Org.BouncyCastle.Cms;

namespace webapp
{
    internal sealed class Signature
    {
        private IBrowserFile _file;
        private CmsSignedData _signature;

        private Signature() {}

        private async Task<Signature> InitializeAsync(IBrowserFile file)
        {
            _file = file;
            byte[] buffer;
            using (var stream = _file.OpenReadStream(FileSize))
            {
                buffer = new byte[stream.Length];
                await stream.ReadAsync(buffer, 0, buffer.Length);
            }
            try
            {
                _signature = new CmsSignedData(buffer);
            }
            catch (Exception ex) {
                Console.WriteLine("Invalid PKCS #7 file {0}: {1}", FileName, ex.Message);
            }
            return this;
        }

        public static Task<Signature> CreateAsync(IBrowserFile file)
        {
            Signature ret = new Signature();
            return ret.InitializeAsync(file);
        }

        public string FileContentType { get => _file.ContentType; }
        public DateTimeOffset FileLastModified { get => _file.LastModified; }
        public string FileName { get => _file.Name; }
        public long FileSize { get => _file.Size; }
        public bool isSignature { get => !(_signature is null); }
        internal CmsSignedData SignatureData { get => _signature; }
    }
}